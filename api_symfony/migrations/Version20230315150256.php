<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230315150256 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bien (id INT AUTO_INCREMENT NOT NULL, type_bien_id INT NOT NULL, type_transaction_id INT DEFAULT NULL, propietaire_id INT DEFAULT NULL, titre VARCHAR(255) NOT NULL, resumer LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, prix DOUBLE PRECISION NOT NULL, adresse VARCHAR(255) NOT NULL, complement VARCHAR(255) NOT NULL, postal INT NOT NULL, commune VARCHAR(255) NOT NULL, lieu VARCHAR(255) NOT NULL, longitude DOUBLE PRECISION NOT NULL, latitude INT NOT NULL, statu TINYINT(1) NOT NULL, nombre_vues INT NOT NULL, INDEX IDX_45EDC38695B4D7FA (type_bien_id), INDEX IDX_45EDC3867903E29B (type_transaction_id), INDEX IDX_45EDC38642546783 (propietaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bien ADD CONSTRAINT FK_45EDC38695B4D7FA FOREIGN KEY (type_bien_id) REFERENCES type_bien (id)');
        $this->addSql('ALTER TABLE bien ADD CONSTRAINT FK_45EDC3867903E29B FOREIGN KEY (type_transaction_id) REFERENCES type_transaction (id)');
        $this->addSql('ALTER TABLE bien ADD CONSTRAINT FK_45EDC38642546783 FOREIGN KEY (propietaire_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE photos ADD bien_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE photos ADD CONSTRAINT FK_876E0D9BD95B80F FOREIGN KEY (bien_id) REFERENCES bien (id)');
        $this->addSql('CREATE INDEX IDX_876E0D9BD95B80F ON photos (bien_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE photos DROP FOREIGN KEY FK_876E0D9BD95B80F');
        $this->addSql('ALTER TABLE bien DROP FOREIGN KEY FK_45EDC38695B4D7FA');
        $this->addSql('ALTER TABLE bien DROP FOREIGN KEY FK_45EDC3867903E29B');
        $this->addSql('ALTER TABLE bien DROP FOREIGN KEY FK_45EDC38642546783');
        $this->addSql('DROP TABLE bien');
        $this->addSql('DROP INDEX IDX_876E0D9BD95B80F ON photos');
        $this->addSql('ALTER TABLE photos DROP bien_id');
    }
}
