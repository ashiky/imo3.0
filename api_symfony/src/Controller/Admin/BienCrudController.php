<?php

namespace App\Controller\Admin;

use App\Entity\Bien;
use App\Entity\TypeBien;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class BienCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Bien::class;
    }

  
    public function configureFields(string $pageName): iterable
    {
        
            yield IdField::new('id')->hideOnForm()->setColumns(3);
            yield TextField::new('titre',"'Nom de l'annonce")->setColumns(3);
            yield AssociationField::new('typeBien',"Le type de votre bien")->setColumns(3);
            yield AssociationField::new('typeTransaction',"Qelle type de transaction")->setColumns(3);
            yield AssociationField::new('propietaire',"Qui est le porpiétaire");
            yield TextField::new('resumer',"Le résumer de votre annonce");
            yield TextField::new('description', "La description détaillé de votre bien");
            yield MoneyField::new('prix',"Le prix de votre bien")->setCurrency("EUR");
            yield TextField::new('adresse',"L'adresse du bien")->setColumns(3);
            yield TextField::new('complement',"'Complements d'ardresse du bien")->setColumns(3);
            yield IntegerField::new('postal','Code postal')->setColumns(3);
            yield TextField::new('commune','La commune');
            yield TextField::new('lieu','Le lieu du bien');
            yield IntegerField::new('longitude')->setColumns(2);
            yield IntegerField::new('latitude')->setColumns(2);
            yield BooleanField::new('statu');
            yield IntegerField::new('nombre_vues');
        
    }

}
