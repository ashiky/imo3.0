<?php

namespace App\Controller\Admin;

use App\Entity\Admin;
use App\Entity\Bien;
use App\Entity\Photos;
use App\Entity\TypeBien;
use App\Entity\TypeTransaction;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{

    public FUNCTION __construct(
        private AdminUrlGenerator $adminUrlGenerator
        ){
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {

        $url =$this->adminUrlGenerator
            ->setController(TypeBienCrudController::class)
            ->generateUrl();
        return $this->redirect($url);

        // return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        //  return $this->render('admin/dashbord.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Imo 3 0');
    }

    // public function configureMenuItems(): iterable
    // {
    //     yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
    //     // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    // }
    public function configureMenuItems(): iterable
    {
       
        yield MenuItem::subMenu("TypeBien","fas fa-bars")->setSubItems([
            MenuItem::linkToCrud("Create","fas fa-plus",TypeBien::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Show","fas fa-eye",TypeBien::class)
        ]);
        yield MenuItem::subMenu("TypeTransaction","fas fa-bars")->setSubItems([
            MenuItem::linkToCrud("Create","fas fa-plus",TypeTransaction::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Show","fas fa-eye",TypeTransaction::class)
        ]);
        yield MenuItem::subMenu("Images","fas fa-bars")->setSubItems([
            MenuItem::linkToCrud("Create","fas fa-plus",Photos::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Show","fas fa-eye",Photos::class)
        ]);
        yield MenuItem::subMenu("User","fas fa-bars")->setSubItems([
            MenuItem::linkToCrud("Create","fas fa-plus",User::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Show","fas fa-eye",User::class)
        ]);
        yield MenuItem::subMenu("Bien","fas fa-bars")->setSubItems([
            MenuItem::linkToCrud("Create","fas fa-plus",Bien::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Show","fas fa-eye",Bien::class)
        ]);
        yield MenuItem::subMenu("Admin","fas fa-bars")->setSubItems([
            MenuItem::linkToCrud("Create","fas fa-plus",Admin::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Show","fas fa-eye",Admin::class)
        ]);
    }
}
