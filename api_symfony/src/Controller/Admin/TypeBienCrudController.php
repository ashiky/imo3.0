<?php

namespace App\Controller\Admin;

use App\Entity\TypeBien;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class TypeBienCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TypeBien::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
            yield IdField::new('id')->hideOnForm();
            yield TextField::new('name');
            yield IntegerField::new("nb")->hideOnForm();
        
    }
   
}
