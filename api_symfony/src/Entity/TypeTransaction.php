<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\TypeTransactionRepository;

#[ORM\Entity(repositoryClass: TypeTransactionRepository::class)]
#[ApiResource]

class TypeTransaction
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $intutule = null;

    #[ORM\Column(nullable: true)]
    private ?int $nb = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntutule(): ?string
    {
        return $this->intutule;
    }

    public function setIntutule(string $intutule): self
    {
        $this->intutule = $intutule;

        return $this;
    }
    public function __toString(){
        return $this->intutule; 
      }

    public function getNb(): ?int
    {
        return $this->nb;
    }

    public function setNb(?int $nb): self
    {
        $this->nb = $nb;

        return $this;
    }
}
