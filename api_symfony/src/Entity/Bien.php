<?php

namespace App\Entity;

use App\Entity\Photos;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\BienRepository;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;


#[ORM\Entity(repositoryClass: BienRepository::class)]
#[ApiResource(
    operations: [
        // Creer
        new Post(normalizationContext: ['groups' => 'bien:item']),
        // show one
        new Get(normalizationContext: ['groups' => 'bien:item']),
        // show all
        new GetCollection(normalizationContext: ['groups' => 'bien:list'])
    ],
    order: ['prix' => 'ASC'],
    paginationEnabled: false,
)]
class Bien
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['bien:list', 'bien:item'])]

    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['bien:list', 'bien:item'])]

    private ?string $titre = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['bien:list', 'bien:item'])]

    private ?TypeBien $typeBien = null;

    #[ORM\ManyToOne]
    #[Groups(['bien:list', 'bien:item'])]

    private ?TypeTransaction $typeTransaction = null;

    #[ORM\ManyToOne(inversedBy: 'bien')]
    #[Groups(['bien:list', 'bien:item'])]

    private ?User $propietaire = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['bien:list', 'bien:item'])]

    private ?string $resumer = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['bien:list', 'bien:item'])]

    private ?string $description = null;

    #[ORM\Column]
    #[Groups(['bien:list', 'bien:item'])]

    private ?float $prix = null;

    #[ORM\Column(length: 255)]
    #[Groups(['bien:list', 'bien:item'])]

    private ?string $adresse = null;

    #[ORM\Column(length: 255)]
    #[Groups(['bien:list', 'bien:item'])]

    private ?string $complement = null;

    #[ORM\Column]
    #[Groups(['bien:list', 'bien:item'])]

    private ?int $postal = null;

    #[ORM\Column(length: 255)]
    #[Groups(['bien:list', 'bien:item'])]

    private ?string $commune = null;

    #[ORM\Column(length: 255)]
    #[Groups(['bien:list', 'bien:item'])]

    private ?string $lieu = null;

    #[ORM\Column]
    #[Groups(['bien:list', 'bien:item'])]

    private ?float $longitude = null;

    #[ORM\Column]
    #[Groups(['bien:list', 'bien:item'])]

    private ?int $latitude = null;

    #[ORM\Column]
    #[Groups(['bien:list', 'bien:item'])]

    private ?bool $statu = null;

    #[ORM\Column]
    #[Groups(['bien:list', 'bien:item'])]

    private ?int $nombreVues = null;

    #[ORM\OneToMany(mappedBy: 'bien', targetEntity: Photos::class)]
    #[Groups(['bien:list', 'bien:item'])]

    private Collection $photos;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getTypeBien(): ?TypeBien
    {
        return $this->typeBien;
    }

    public function setTypeBien(?TypeBien $typeBien): self
    {
        $this->typeBien = $typeBien;

        return $this;
    }

    public function getTypeTransaction(): ?TypeTransaction
    {
        return $this->typeTransaction;
    }

    public function setTypeTransaction(?TypeTransaction $typeTransaction): self
    {
        $this->typeTransaction = $typeTransaction;

        return $this;
    }

    public function getPropietaire(): ?User
    {
        return $this->propietaire;
    }

    public function setPropietaire(?User $propietaire): self
    {
        $this->propietaire = $propietaire;

        return $this;
    }

    public function getResumer(): ?string
    {
        return $this->resumer;
    }

    public function setResumer(?string $resumer): self
    {
        $this->resumer = $resumer;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(string $complement): self
    {
        $this->complement = $complement;

        return $this;
    }

    public function getPostal(): ?int
    {
        return $this->postal;
    }

    public function setPostal(int $postal): self
    {
        $this->postal = $postal;

        return $this;
    }

    public function getCommune(): ?string
    {
        return $this->commune;
    }

    public function setCommune(string $commune): self
    {
        $this->commune = $commune;

        return $this;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(string $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?int
    {
        return $this->latitude;
    }

    public function setLatitude(int $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function isStatu(): ?bool
    {
        return $this->statu;
    }

    public function setStatu(bool $statu): self
    {
        $this->statu = $statu;

        return $this;
    }

    public function getNombreVues(): ?int
    {
        return $this->nombreVues;
    }

    public function setNombreVues(int $nombreVues): self
    {
        $this->nombreVues = $nombreVues;

        return $this;
    }

    /**
     * @return Collection<int, Photos>
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photos $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos->add($photo);
            $photo->setBien($this);
        }

        return $this;
    }

    public function removePhoto(Photos $photo): self
    {
        if ($this->photos->removeElement($photo)) {
            // set the owning side to null (unless already changed)
            if ($photo->getBien() === $this) {
                $photo->setBien(null);
            }
        }

        return $this;
    }
}
