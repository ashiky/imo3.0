<?php

namespace App\DataFixtures;

use Faker;
use Faker\Factory;
use App\Entity\Bien;
use App\Entity\Photos;
use App\Entity\User;
use App\Entity\TypeBien;
use App\Entity\TypeTransaction;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
   public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $phrase = "";
        

        // create 20 products! Bam!
        for ($i = 0; $i < 5; $i++) {
            $typeBien = new TypeBien();
            $typeBien->setName($faker->word());
            $manager->persist($typeBien);
        }
        for ($i = 0; $i < 5; $i++) {
            $typeTransaction = new TypeTransaction();
            $typeTransaction->setIntutule($faker->word);
            $manager->persist($typeTransaction);
        }
        for ($i = 0; $i < 5; $i++) {
            $user = new User();
            $user->setNom($faker->lastName());
            $user->setPrenom($faker->firstName());
            $user->setEmail($faker->email());
            $user->setRoles([]);
            $user->setPassword("password");
            $user->setCivilite("M");
            $user->setAdresse("$i rue de l'exemple");
            $user->setComplementAdresse("Batiment $i");
            $user->setCodePostal($faker->randomNumber(5, true));
            $user->setCommune("Oloron");
            $user->setPortable(0000000000);
            $user->setFixe(0000000000);
            $manager->persist($user);
        }
        
        for ($i = 0; $i < 20; $i++) {
            $bien = new Bien();
            $bien->setTitre($faker->lastName());
            $bien->setTypeBien($typeBien);
            $bien->setTypeTransaction($typeTransaction);
            $bien->setPropietaire($user);
            $bien->setResumer($faker->word());
            for( $a = 0;$a < 50 ; $a++){
                    $phrase = $phrase . $faker->word() . " ";
            }
            $bien->setDescription($phrase);
            $bien->setPrix($faker->randomNumber(3, true));
            $bien->setAdresse("$i rue de l'exemple");
            $bien->setComplement("Batiment $i");
            $bien->setPostal($faker->randomNumber(5, true));
            $bien->setCommune("Oloron");
            $bien->setLieu("Oloron");
            $bien->setLatitude($faker->randomNumber(5, true));
            $bien->setLongitude($faker->randomNumber(5, true));
            $bien->setStatu(false);
            $bien->setNombreVues($faker->randomNumber(2, true));



            $manager->persist($bien);

        }
        for ($i = 0; $i < 5; $i++) {
            $image = new Photos();
            $image->setFichier("https://easydrawingguides.com/wp-content/uploads/2022/01/Simple-House-step-by-step-drawing-tutorial-step-10.png");
            $image->setTitre($faker->word());
            $image->setBien($bien);
            $manager->persist($image);
        }

        $manager->flush();
    }
}
