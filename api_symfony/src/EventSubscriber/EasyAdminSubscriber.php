<?php

namespace App\EventSubscriber;

use App\Entity\Bien;
use App\Repository\TypeBienRepository;
use App\Repository\TypeTransactionRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityDeletedEvent;
;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    private $TypeBienRepository;
    private $TypeTransactionRepository;
    private $em;



    public function __construct(TypeBienRepository $TypeBienRepository, TypeTransactionRepository $TypeTransactionRepository, EntityManagerInterface $em)
    {
        $this->TypeBienRepository = $TypeBienRepository;
        $this->TypeTransactionRepository = $TypeTransactionRepository;
        $this->em =  $em;
    }

    public static function getSubscribedEvents()
    {
        return [
            AfterEntityPersistedEvent
::class => ['increment'],
            AfterEntityDeletedEvent::class => ['decrement'],

        ];
    }

    public function increment(AfterEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        
        if (!($entity instanceof Bien)) {
            return;
        }

        $typeBien = $this->TypeBienRepository->find($entity->getTypeBien());
        $nb = $typeBien->getNb() + 1;
        $typeBien->setNb($nb);
        $this->em->persist($typeBien);

        $typeTransaction = $this->TypeTransactionRepository->find($entity->getTypeTransaction());
        $nb = $typeTransaction->getNb() + 1;
        $typeTransaction->setNb($nb);
        $this->em->persist($typeTransaction);

        $this->em->flush();
    }
    public function decrement(AfterEntityDeletedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Bien)) {
            return;
        }

        $typeBien = $this->TypeBienRepository->find($entity->getTypeBien());
        $nb = $typeBien->getNb() -1;
        $typeBien->setNb($nb);
        $this->em->persist($typeBien);

        $typeTransaction = $this->TypeTransactionRepository->find($entity->getTypeTransaction());
        $nb = $typeTransaction->getNb() - 1;
        $typeTransaction->setNb($nb);
        $this->em->persist($typeTransaction);

        $this->em->flush();
    }
}