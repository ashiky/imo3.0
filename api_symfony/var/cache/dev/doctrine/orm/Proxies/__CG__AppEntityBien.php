<?php

namespace Proxies\__CG__\App\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Bien extends \App\Entity\Bien implements \Doctrine\Persistence\Proxy
{
    use \Symfony\Component\VarExporter\LazyGhostTrait {
        initializeLazyObject as __load;
        setLazyObjectAsInitialized as public __setInitialized;
        isLazyObjectInitialized as private;
        createLazyGhost as private;
        resetLazyObject as private;
        __clone as private __doClone;
    }

    private const LAZY_OBJECT_PROPERTY_SCOPES = [
        "\0".parent::class."\0".'adresse' => [parent::class, 'adresse', null],
        "\0".parent::class."\0".'commune' => [parent::class, 'commune', null],
        "\0".parent::class."\0".'complement' => [parent::class, 'complement', null],
        "\0".parent::class."\0".'description' => [parent::class, 'description', null],
        "\0".parent::class."\0".'id' => [parent::class, 'id', null],
        "\0".parent::class."\0".'latitude' => [parent::class, 'latitude', null],
        "\0".parent::class."\0".'lieu' => [parent::class, 'lieu', null],
        "\0".parent::class."\0".'longitude' => [parent::class, 'longitude', null],
        "\0".parent::class."\0".'nombreVues' => [parent::class, 'nombreVues', null],
        "\0".parent::class."\0".'photos' => [parent::class, 'photos', null],
        "\0".parent::class."\0".'postal' => [parent::class, 'postal', null],
        "\0".parent::class."\0".'prix' => [parent::class, 'prix', null],
        "\0".parent::class."\0".'propietaire' => [parent::class, 'propietaire', null],
        "\0".parent::class."\0".'resumer' => [parent::class, 'resumer', null],
        "\0".parent::class."\0".'statu' => [parent::class, 'statu', null],
        "\0".parent::class."\0".'titre' => [parent::class, 'titre', null],
        "\0".parent::class."\0".'typeBien' => [parent::class, 'typeBien', null],
        "\0".parent::class."\0".'typeTransaction' => [parent::class, 'typeTransaction', null],
        'adresse' => [parent::class, 'adresse', null],
        'commune' => [parent::class, 'commune', null],
        'complement' => [parent::class, 'complement', null],
        'description' => [parent::class, 'description', null],
        'id' => [parent::class, 'id', null],
        'latitude' => [parent::class, 'latitude', null],
        'lieu' => [parent::class, 'lieu', null],
        'longitude' => [parent::class, 'longitude', null],
        'nombreVues' => [parent::class, 'nombreVues', null],
        'photos' => [parent::class, 'photos', null],
        'postal' => [parent::class, 'postal', null],
        'prix' => [parent::class, 'prix', null],
        'propietaire' => [parent::class, 'propietaire', null],
        'resumer' => [parent::class, 'resumer', null],
        'statu' => [parent::class, 'statu', null],
        'titre' => [parent::class, 'titre', null],
        'typeBien' => [parent::class, 'typeBien', null],
        'typeTransaction' => [parent::class, 'typeTransaction', null],
    ];

    /**
     * @internal
     */
    public bool $__isCloning = false;

    public function __construct(?\Closure $initializer = null)
    {
        self::createLazyGhost($initializer, [
            "\0".parent::class."\0".'id' => true,
            '__isCloning' => true,
        ], $this);
    }

    public function __isInitialized(): bool
    {
        return isset($this->lazyObjectState) && $this->isLazyObjectInitialized();
    }

    public function __clone()
    {
        $this->__isCloning = true;

        try {
            $this->__doClone();
        } finally {
            $this->__isCloning = false;
        }
    }

    public function __serialize(): array
    {
        $properties = (array) $this;
        unset($properties["\0" . self::class . "\0lazyObjectState"], $properties['__isCloning']);

        return $properties;
    }
}
