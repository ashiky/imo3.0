import { IBiens } from "../../App"
import { IPhotos } from "../../App"

type annonceProps = {
    bien:IBiens
    photos:IPhotos[]
}




function annonce(props:annonceProps){
    return (
        <div className="annonce">
            
            <img src={props?.photos[0]?.fichier} alt="" />
            <div className="detailAnnonce">
                <h2>Titre : {props.bien.titre}</h2>
                <p>Proprietaire : {props.bien.propietaire.prenom + " " + props.bien.propietaire.nom}</p>
                <p>Prix : {props.bien.prix}</p>
                <div>
                    <p>Adresse : {props.bien.adresse}</p>
                    <p>Complement : {props.bien.complement}</p>
                    <p>Code Postal : {props.bien.postal}</p>
                </div>
            </div>
        </div>
    )
}

export default annonce