import { IUser } from "../../App";
import { useState,useEffect } from "react"
import axios from "axios";

interface ITypeBien {
    name:string
    id:number
}

interface ITypeTransaction {
    intutule:string
    id:number
}

type formAnnonceProps = {
    users:IUser[]
}

export default function FormAnnonce(props:formAnnonceProps){
    const [titre , setTitre] = useState<string>("");
    const [resumer , setResumer] = useState<string>("");
    const [description , setDescription] = useState<string>("");
    const [prix , setprix] = useState<number>(0);
    const [adresse , setAdresse] = useState<string>("");
    const [complement , setComplement] = useState<string>("");
    const [postal , setPostal] = useState<string>("");
    const [commune , setCommune] = useState<string>("");
    const [lieu , setLieu] = useState<string>("");
    const [longitude , setLongitude] = useState<number>(0);
    const [latitude , setLatitude] = useState<number>(0);
    const [statu , setStatu] = useState<boolean>(false);
    const [nombreVues , setNombreVues] = useState<number>(0);
    const [typeTransaction , settypeTransaction] = useState<string>("");
    const [typeBien , setTypeBien] = useState<string>("");
    const [propietaire , setPropietaire] = useState<IUser>();

    const [typeBiens , setTypeBiens] = useState<ITypeBien[]>([]);
    const   [typeTransactions , setTypeTransactions] = useState<ITypeTransaction[]>([]);


     const fetchTypeBiens = async()=> {
           const res = await axios.get("http://127.0.0.1:8000/api/type_biens?page=1")
        setTypeBiens(res.data["hydra:member"])      
      }

    const fetchTypeTransactions = async()=> {
      const res = await axios.get("http://127.0.0.1:8000/api/type_transactions?page=1")
      setTypeTransactions(res.data["hydra:member"])      
    }

    useEffect(()=>{
        fetchTypeBiens()
        fetchTypeTransactions()
    }, [])


    async function postData(url = "http://127.0.0.1:8000/api/biens", data = {
        titre:{adresse},
        resumer:{resumer},
        description:{description},
        prix:{prix},
        adresse:{adresse},
        complement:{complement},
        postal:{postal},
        commune:{commune},
        lieu:{lieu},
        longitude:{longitude},
        latitude:{latitude},
        statu:{statu},
        nombreVues:{nombreVues},
        typeBien:{typeBien},
        typeTransaction:{typeTransaction},
        propietaire:{propietaire}
    }) {
        const response = await fetch(url, {
          method: "POST", 
          mode: "cors", 
          cache: "no-cache", 
          credentials: "same-origin", 
          headers: {
            "Content-Type": "application/json",
          },
          redirect: "follow", 
          referrerPolicy: "no-referrer",
          body: JSON.stringify(data),
        });
        return response.json(); 
      }
      postData("http://127.0.0.1:8000/api/biens").then((data) => {
        console.log(data); 
      });
    
      var newAnnonce = function(event:any) {
        event.preventDefault();
        console.log(postData())
    };

    return(
        <div>
            <form onSubmit={newAnnonce} action="" method="post" className="m-3">
                <p className="m-2 h5">Titre :</p>
                <input className="form-control form-control-lg" type="text" placeholder="Entrez le titre de votre annonce" aria-label="Titre" />
                <div className="d-flex justify-content-around m-2">
                    <div className="d-flex">
                        <p className="m-2 h5">Type de transaction :</p>
                        <select className="select">
                            {typeTransactions.map((tt)=>{
                                return(
                                    <option key={tt.id}>{tt.intutule}</option>            
                                )
                            })}
                        </select>
                    </div>
                    <div className="d-flex">
                        <p className="m-2 h5">Type de bien :</p>
                        <select className="select">
                        {typeBiens.map((tb)=>{
                            return(
                                <option key={tb.id}>{tb.name}</option>            
                            )
                        })}
                        </select>
                    </div>                
                </div>
                <p className="m-2 h5">Proprietaire :</p>
                    <select className="select">
                        {props.users.map((user)=>{
                            return(
                                <option key={user.id}>{user.prenom + " " + user.nom}</option>            
                            )
                        })}
                        </select>
                <p className="m-2 h5">resumer :</p>
                <input
                    onChange={(resumer) => setResumer(resumer.target.value)} 
                    name="resumer" 
                    className="form-control" 
                    type="text" 
                    placeholder="Default input" 
                    aria-label="default input example">
                </input>
                <p className="m-2 h5">Description :{resumer}</p>
                <input
                 onChange={(description) => setDescription(description.target.value)} 
                    name="description" 
                    className="form-control form-control-sm" 
                    type="text" placeholder=".form-control-sm" 
                    aria-label=".form-control-sm example" />
                <p className="m-2 h5">Prix :</p>
                <input 
                    onChange={(prix) => setprix(Number(prix.target.value))}
                    name="prix" 
                    className="form-control form-control-lg" 
                    type="text" 
                    placeholder=".form-control-lg" 
                    aria-label=".form-control-lg example">
                </input>    
                <p className="m-2 h5">Adresse :</p>
                <input 
                    onChange={(adresse) => setAdresse(adresse.target.value)}
                    name="adresse" 
                    className="form-control" 
                    type="text" 
                    placeholder="Default input" 
                    aria-label="default input example">
                </input>    
                <p className="m-2 h5">Complement d'adresse :</p>
                <input 
                 onChange={(complement) => setComplement(complement.target.value)}
                    name="complement" 
                    className="form-control form-control-sm" 
                    type="text" 
                    placeholder=".form-control-sm" 
                    aria-label=".form-control-sm example">
                </input>
                <p className="m-2 h5">Code postal :</p>
                <input 
                 onChange={(postal) => setPostal(postal.target.value)}
                    name="postal" 
                    className="form-control form-control-lg" 
                    type="text" 
                    placeholder=".form-control-lg" 
                    aria-label=".form-control-lg example" >
                </input>
                <p className="m-2 h5">Commune :</p>
                <input 
                    onChange={(commune) => setCommune(commune.target.value)}
                    name="commune" 
                    className="form-control" 
                    type="text" 
                    placeholder="Default input" 
                    aria-label="default input example">
                </input>
                <p className="m-2 h5">Lieu :</p>
                <input 
                    onChange={(lieu) => setLieu(lieu.target.value)}
                    name="lieu" 
                    className="form-control form-control-sm" 
                    type="text" 
                    placeholder=".form-control-sm" 
                    aria-label=".form-control-sm example" >
                </input>
                <p className="m-2 h5">Longitude :</p>
                <input 
                    onChange={(longitude) => setLongitude(Number(longitude.target.value))}
                    name="longitude" 
                    className="form-control" 
                    type="text" 
                    placeholder="Default input" 
                    aria-label="default input example" >
                </input>
                <p className="m-2 h5">Latitude :</p>
                <input 
                     onChange={(latitude) => setLatitude(Number(latitude.target.value))}
                    name="latitude" 
                    className="form-control form-control-sm" 
                    type="text" 
                    placeholder=".form-control-sm" 
                    aria-label=".form-control-sm example" >  
                </input>
                <p className="m-2 h5">Statu :</p>
                <input 
                    onChange={(statu) => setStatu(statu.target.checked)}
                    name="statu"  
                    type="checkbox">
                </input>
                <p className="m-2 h5">Nombre de vue :</p>
                <input 
                     onChange={(nombreVues) => setNombreVues(Number(nombreVues.target.value))}
                    name="nombreVues" 
                    className="form-control" 
                    type="text" 
                    placeholder="Default input" 
                    aria-label="default input example"> 
                </input>
                <p className="m-2 h5">Photo :</p>
               
                <button>Envoyer</button>
            </form>
        </div>
    )
}