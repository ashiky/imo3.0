import { useState,useEffect } from "react"
import { IBiens } from "../../App"
import { IPhotos } from "../../App";
import { useParams } from "react-router-dom";
import axios from "axios";
import Carousel from 'react-bootstrap/Carousel';
type detailBienProps = {
    idBien: number 
    photos: IPhotos[]
}

   

function detailAnnonce(props:detailBienProps){
    const [detailBiens , setDetailBiens] = useState<IBiens>();
    const params = useParams()
    console.log(params)
    const fetchDetailBiens = async()=> {
        const res = await axios.get("http://127.0.0.1:8000/api/biens/" + params.id)            
            setDetailBiens(res.data)                   
    }
    //   console.log(detailBiens)
    useEffect( () => { 
        fetchDetailBiens()
    }, [])
    return(
        <div>
            <Carousel className="carousel">
                {props.photos.map((photo,id) => {
                    return (
                        
                            <Carousel.Item key={id} className="slides">
                                <img key={id} className="d-block w-100" src={photo.fichier} alt="First slide"/>
                            </Carousel.Item>
                          
                    )
                })}
            </Carousel>
        <div className="m-5">
            <h1>{detailBiens?.titre}</h1>
            <div className="d-flex">
                <p className="m-1"><strong>Commune</strong> : {detailBiens?.commune} </p>
                <p className="m-1"><strong> Prix</strong> : {detailBiens?.prix} $</p>
            </div>
            <p className="m-1"><strong>Proprietaire</strong> : {detailBiens?.propietaire.prenom + " " + detailBiens?.propietaire.nom}</p>
            <div>
                <h3 className="h6 m-1"><strong>Description</strong> :</h3>
                <p className="m-2">{detailBiens?.description}</p>
            </div>
            <div>
                <p className="m-1"><strong>Adresse</strong> : {detailBiens?.adresse}</p>
                <p className="m-1"><strong>Lieu</strong> : {detailBiens?.lieu}</p>
            </div>
        </div>
    </div>
    )
}


export default detailAnnonce
 // <h1>{detailBiens?.propietaire.prenom}</h1>

 