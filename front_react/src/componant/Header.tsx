import { Link } from "react-router-dom"

type headerProps = {
    setSearch: Function
}

function header(props:headerProps){
    return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-around" id="navbarNav">
                    <ul className="navbar-nav justify-content-around w-100">
                        <li className="nav-item active">
                            <Link className="h2" to={"/"}>Accueil</Link>                        
                        </li>
                        <li className="nav-item">
                            <Link to={"/DetailAnnonce"}>Mes annonces</Link>
                        </li>
                        <li>
                        <input 
                            type="search"
                            placeholder="Rechercher..."
                            onChange={(event) => props.setSearch(event.target.value)} />
                        </li>
                    </ul>
                </div>
            </nav>
    )
}


export default header