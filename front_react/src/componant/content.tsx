import { IBiens } from "../App";
import Annonce from "./annonce/Annonce"
import Accueil from "./Accueil";
import { IPhotos } from "../App";
import {Link} from "react-router-dom"
import DetailAnnonce from "./annonce/DetailAnnonce";
type contentProps = {
    biens:IBiens[]
    photos:IPhotos[]
    search:string
}

function content(props:contentProps){
    return (
     <Accueil>
      <Link to={"/annonce/formulaire"}>Ajouter une annonce</Link>
        
        {props.biens.filter((biens)=>{
          if(props.search==""){
            return biens
          } else if(biens.titre.toLocaleLowerCase().includes(props.search.toLocaleLowerCase())){
            return biens
          }
        }).map((bien) => {
          return (
            <div key={bien.id}>
                <Link to={`annonce/detail/${bien.id}`}> 
                  <Annonce photos={props.photos} bien={bien}/>
                </Link>
            </div>
          )
         })}
      </Accueil>
     
    )

}
export default content

