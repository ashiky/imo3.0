import { ReactNode } from "react"
import Annonce from "./annonce/Annonce"

type accueilProps = {
    children?:ReactNode
}

function accueil(props:accueilProps){
    return (
        <div className="accueil">
          {props.children}
        </div>
    )
}


export default accueil