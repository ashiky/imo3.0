import './App.css'
import { useState, useEffect } from "react";
import axios from "axios";
import {Routes, Route} from "react-router-dom"
import Content from "./componant/content"
import FormAnnonce from "./componant/annonce/FormAnnonce"
import Header from "./componant/Header"
import DetailAnnonce from './componant/annonce/DetailAnnonce';
export interface IBiens {
  id:number
  titre:string
  resumer:string
  description:string
  prix:GLfloat
  adresse:string
  complement:string
  postal:number
  commune:string
  lieu:string
  longitude:GLfloat
  latitude:GLfloat
  statu:boolean
  nombreVues:number
  propietaire:{
    id:number
    nom:string
    prenom:string
  }
}

export interface IPhotos {
  titre:string
  fichier:string
  bien_id:number
  id:number
}

export interface IUser {
  id:number
  email:string
  roles:[]
  civilite:string
  nom:string
  prenom:string
  adresse:string
  complement_adresse:string
  code_postal:string
  commune:string
  portable:number
  fixe:number
}

function App() {
  const [biens , setBiens] = useState<IBiens[]>([]);
  const [photos , setPhotos] = useState<IPhotos[]>([]);
  const [users , setUsers] = useState<IUser[]>([]);

  const [search, setSearch] = useState("")
  const fetchBiens = async()=> {
    const res = await axios.get("http://127.0.0.1:8000/api/biens")
    setBiens(res.data["hydra:member"])      
  }

    
  const fetchPhotos = async()=> {
    const res = await axios.get("http://127.0.0.1:8000/api/photos?page=1")
    setPhotos(res.data["hydra:member"])      
  }

  const fetchUsers = async()=> {
    const res = await axios.get("http://127.0.0.1:8000/api/users?page=1")
    setUsers(res.data["hydra:member"])      
  }


  // console.log(biens[0])
  // console.log(photos)
  // console.log(users)
  useEffect( () => { 
    fetchBiens()
    fetchPhotos()
    fetchUsers() 
  } , [])
  
  return (
    <div className="App">
      <Header setSearch={setSearch} />
      <Routes>
        <Route path='/' element={ <Content photos= { photos } biens= { biens } search={search} /> }/>
        <Route path='/annonce/detail/:id' element= { <DetailAnnonce photos= { photos } idBien= { 215 } /> } />
        <Route path='/annonce/formulaire' element= { <FormAnnonce users={users}/> }></Route>

      </Routes>
    </div>
  )
}

export default App
